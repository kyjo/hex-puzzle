package tkyjovsk.hexpuzzle;

/**
 *
 * @author tkyjovsk
 */
public class Solution {

    public static final Constraints initialConstraints
            = new Constraints(7,
                    new String[]{
                        "(ND|ET|IN)[^X]*",
                        "[CHMNOR]*I[CHMNOR]*",
                        "P+(..)\\1.*",
                        "(E|CR|MN)*",
                        "([^MC]|MM|CC)*",
                        "[AM]*CM(RC)*R?",
                        ".*",
                        ".*PRR.*DDC.*",
                        "(HHX|[^HX])*",
                        "([^EMC]|EM)*",
                        ".*OXR.*",
                        ".*LR.*RL.*",
                        ".*SE.*UE.*",},
                    new String[]{
                        ".*H.*H.*",
                        "(DI|NS|TH|OM)*",
                        "F.*[AO].*[AO].*",
                        "(O|RHH|MM)*",
                        ".*",
                        "C*MC(CCC|MM)*",
                        "[^C]*[^R]*III.*",
                        "(...?)\\1*",
                        "([^X]|XCC)*",
                        "(RR|HHH)*.?",
                        "N.*X.X.X.*E",
                        "R*D*M*",
                        ".(C|HH)*",},
                    new String[]{
                        ".*G.*V.*H.*",
                        "[CR]*",
                        ".*XEXM*",
                        ".*DD.*CCM.*",
                        ".*XHCR.*X.*",
                        ".*(.)(.)(.)(.)\\4\\3\\2\\1.*",
                        ".*(IN|SE|HI)",
                        "[^C]*MMM[^C]*",
                        ".*(.)C\\1X\\1.*",
                        "[CEIMU]*OH[AEMOR]*",
                        "(RX|[^R])*",
                        "[^M]*M[^M]*",
                        "(S|MM|HHH)*",});

    public static HexaGrid grid = new HexaGrid(7);

    public static void main(String[] args) throws InterruptedException {

        KnownFields.injectIntoGrid(grid);

        System.out.println(grid);

        Constraints constraints = initialConstraints;

        int stepLimit = 15;
        double possibleCombinations = grid.countPossibileCombinations();

        for (int step = 0; step < stepLimit; step++) {
            System.out.println("\nITERATION " + (step + 1) + ".\n");

            constraints.generateStrings(1000);

            grid.applyConstraints(constraints);
            System.out.println("Grid:\n" + grid);

            double cutCombinations = possibleCombinations - grid.countPossibileCombinations();
            System.out.println(cutCombinations + " combinations were cut off since the previous iteration.\n");
            if (cutCombinations <= 0) {
                System.out.println("Terminating.");
                break;
            }

            possibleCombinations = grid.getPossibileCombinations();
            if (possibleCombinations == 1) {
                System.out.println("Puzzle solved!\n");

                System.out.println("Verifying results:");

                Constraints solutionConstraints = grid.constructConstraintsFromGrid();
                solutionConstraints.generateStrings();
                
                solutionConstraints.verifyStringsAgainst(initialConstraints);

                System.out.println("\n"
                        + grid.toSolutionString());

                break;
            }

            constraints = grid.constructConstraintsFromGrid().intersectWithRegExpsFrom(constraints);
        }

    }

}
