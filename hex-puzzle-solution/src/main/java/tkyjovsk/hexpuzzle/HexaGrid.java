package tkyjovsk.hexpuzzle;

import java.util.HashSet;
import java.util.Set;
import org.junit.Assert;

/**
 *
 * @author tkyjovsk
 */
public final class HexaGrid {

    private final int size;
    private final Field[][] grid;
    private double possibleCombinations = 0;

    public HexaGrid(int size) {
        Assert.assertTrue(size > 0);
        this.size = size;
        grid = new Field[2 * size - 1][2 * size - 1];
        for (int y = -size + 1; y < size; y++) {
            for (int x = -size + 1; x < size; x++) {
                if (isInGrid(x, y)) {
                    int j = gridToArrayCoord(y);
                    int i = gridToArrayCoord(x);
                    grid[i][j] = new Field(x, y);
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int y = -size + 1; y < size; y++) {
            for (int x = -size + 1; x < size; x++) {
                if (isInGrid(x, y)) {
                    Field f = getFieldByGridCoords(x, y);
                    String s = "";
                    if (f.getCharacters().size() <= 6) {
                        for (Character ch : f.getCharacters()) {
                            sb.append(ch);
                        }
                    } else {
                        s = String.valueOf(f.getCharacters().size());
                    }
                    sb.append(s).append("\t");
                } else {
                    sb.append("  \t");
                }
            }
            sb.append('\n');
        }

        sb.append(countPossibileCombinations()).append(" possible combinations\n");
        return sb.toString();
    }

    public String toSolutionString() {
        StringBuilder sb = new StringBuilder();
        for (int y = -size + 1; y < size; y++) {
            for (int sp = 0; sp < Math.abs(y); sp++) {
                sb.append(" ");
            }
            for (int x = -size + 1; x < size; x++) {
                if (isInGrid(x, y)) {
                    Field f = getFieldByGridCoords(x, y);
                    if (f.getCharacters().size() == 1) {
                        Character c = f.getCharacters().iterator().next();
                        sb.append(c).append(" ");
                    } else {
                        sb.append("? ");
                    }
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }

    public double getPossibileCombinations() {
        return possibleCombinations;
    }

    public double countPossibileCombinations() {
        double combinations = 1;
        for (int i = 0; i < 2 * size - 1; i++) {
            for (int j = 0; j < 2 * size - 1; j++) {
                if (isInGridByArrayCoords(i, j)) {
                    combinations = combinations * getField(i, j).getCharacters().size();
                }
            }
        }
        this.possibleCombinations = combinations;
        return this.possibleCombinations;
    }

    public Field getField(int i, int j) {
        return grid[i][j];
    }

    public Field getFieldByGridCoords(int x, int y) {
        return getField(gridToArrayCoord(x), gridToArrayCoord(y));
    }

    public static int gridToArrayCoord(int gridCoord, int size) {
        return gridCoord + size - 1;
    }

    public int gridToArrayCoord(int gridCoord) {
        return gridToArrayCoord(gridCoord, getSize());
    }

    public static int arrayToGridCoord(int arrayCoord, int size) {
        return arrayCoord - size + 1;
    }

    public int arrayToGridCoord(int arrayCoord) {
        return arrayToGridCoord(arrayCoord, getSize());
    }

    public boolean isInGridByArrayCoords(int i, int j) {
        return isInGrid(arrayToGridCoord(i), arrayToGridCoord(j));
    }

    public boolean isInGrid(int x, int y) {
        boolean a = Math.abs(y) < getSize();
        boolean b = y < getSize() && y > -getSize() + x;
        boolean c = y > -getSize() && y < getSize() + x;
        return x == 0 ? a : (x > 0 ? b : c);
    }

    public static int getLengthByArrayCoord(int arrayCoord, int size) {
        return getLengthByGridCoord(arrayToGridCoord(arrayCoord, size), size);
    }

    public static int getLengthByGridCoord(int gridCoord, int size) {
        return (2 * size - 1) - Math.abs(gridCoord);
    }

    public int getLengthByArrayCoord(int arrayCoord) {
        return getLengthByArrayCoord(arrayCoord, size);
    }

    public int getLengthByGridCoord(int coord) {
        return getLengthByGridCoord(coord, size);
    }

    public static int getDCoord(int x, int y) {
        return x - y;
    }

    public int getXCharPosition(int x, int y) {
        int i = gridToArrayCoord(x);
        int j = gridToArrayCoord(y);
        return i < size ? j : j - (i - size + 1);
    }

    public int getYCharPosition(int x, int y) {
        int i = gridToArrayCoord(x);
        int j = gridToArrayCoord(y);
        return j < size ? i : i - (j - size + 1);
    }

    public int getDCharPosition(int x, int y) {
        int i = gridToArrayCoord(x);
        int j = gridToArrayCoord(y);
        int maxPos = 2 * (size - 1);
        int d = Math.max(i, j);
        return maxPos - d;
    }

    public Field getFieldByXCharPosition(int arrayCoord, int xPos) {
        int i = arrayCoord;
        int j = i < size ? xPos : xPos + (i - size + 1);
        return getField(i, j);
    }

    public Field getFieldByYCharPosition(int arrayCoord, int yPos) {
        int j = arrayCoord;
        int i = j < size ? yPos : yPos + (j - size + 1);
        return getField(i, j);
    }

    public Field getFieldByDCharPosition(int arrayCoord, int dPos) {
        int maxPos = 2 * (size - 1);
        int i = arrayCoord < size ? arrayCoord + size - 1 - dPos : maxPos - dPos;
        int j = arrayCoord < size ? maxPos - dPos : maxPos - (arrayCoord - size + 1) - dPos;
        return getField(i, j);
    }

    public int getSize() {
        return size;
    }

    /** 
     * Removes all characters from grid fields that don't match constraints.     * 
     * @param constraints must have generated string sets for regular expressions
     */
    public void applyConstraints(Constraints constraints) {
        
        System.out.println("Applying constraints.\n");

        Assert.assertTrue(constraints != null && constraints.getSize() == size);

        for (int y = -size + 1; y < size; y++) {
            for (int x = -size + 1; x < size; x++) {
                if (isInGrid(x, y)) {
                    Field f = getFieldByGridCoords(x, y);
                    int i = gridToArrayCoord(x);
                    int j = gridToArrayCoord(y);
                    int d = gridToArrayCoord(f.getD());
                    Set<String> xStrings = constraints.getxStrings()[i];
                    Set<String> yStrings = constraints.getyStrings()[j];
                    Set<String> dStrings = constraints.getdStrings()[d];
                    if (xStrings != null) {
                        Set<Character> validXChars = new HashSet<>();
                        int xPos = getXCharPosition(x, y);
                        for (String xS : xStrings) {
                            validXChars.add(xS.charAt(xPos));
                        }
                        f.getCharacters().retainAll(validXChars);
                    }
                    if (yStrings != null) {
                        Set<Character> validYChars = new HashSet<>();
                        int yPos = getYCharPosition(x, y);
                        for (String yS : yStrings) {
                            validYChars.add(yS.charAt(yPos));
                        }
                        f.getCharacters().retainAll(validYChars);
                    }
                    if (dStrings != null) {
                        Set<Character> validDChars = new HashSet<>();
                        int xPos = getDCharPosition(x, y);
                        for (String dS : dStrings) {
                            validDChars.add(dS.charAt(xPos));
                        }
                        f.getCharacters().retainAll(validDChars);
                    }
                }
            }
        }

        countPossibileCombinations();
    }

    /**
     * Creates regular expressions from grid fields/lines.     *
     * @return
     */
    public Constraints constructConstraintsFromGrid() {
        String[] xRegExps = new String[2 * size - 1];
        String[] yRegExps = new String[2 * size - 1];
        String[] dRegExps = new String[2 * size - 1];

        for (int coord = 0; coord < 2 * size - 1; coord++) {
            StringBuilder sb = new StringBuilder();
            for (int pos = 0; pos < getLengthByArrayCoord(coord, size); pos++) {
                Field f = getFieldByXCharPosition(coord, pos);
                sb.append(f.toRegExp());
            }
            xRegExps[coord] = sb.toString();
        }
        for (int coord = 0; coord < 2 * size - 1; coord++) {
            StringBuilder sb = new StringBuilder();
            for (int pos = 0; pos < getLengthByArrayCoord(coord, size); pos++) {
                Field f = getFieldByYCharPosition(coord, pos);
                sb.append(f.toRegExp());
            }
            yRegExps[coord] = sb.toString();
        }
        for (int coord = 0; coord < 2 * size - 1; coord++) {
            StringBuilder sb = new StringBuilder();
            for (int pos = 0; pos < getLengthByArrayCoord(coord, size); pos++) {
                Field f = getFieldByDCharPosition(coord, pos);
                sb.append(f.toRegExp());
            }
            dRegExps[coord] = sb.toString();
        }

        return new Constraints(size, xRegExps, yRegExps, dRegExps);
    }

}
