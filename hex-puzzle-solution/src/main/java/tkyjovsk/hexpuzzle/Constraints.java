package tkyjovsk.hexpuzzle;

import dk.brics.automaton.Automaton;
import dk.brics.automaton.RegExp;
import java.util.HashSet;
import java.util.Set;
import static tkyjovsk.hexpuzzle.HexaGrid.*;

/**
 *
 * @author tkyjovsk
 */
import org.junit.Assert;

public class Constraints {

    private final int size;

    private final String[] xRegExps;
    private final String[] yRegExps;
    private final String[] dRegExps;

    private final Set<String>[] xStrings;
    private final Set<String>[] yStrings;
    private final Set<String>[] dStrings;

    public Constraints(int size, String[] xRegExps, String[] yRegExps, String[] dRegExps) {
        Assert.assertTrue(size > 0);
        Assert.assertTrue(xRegExps != null && xRegExps.length == 2 * size - 1);
        Assert.assertTrue(yRegExps != null && yRegExps.length == 2 * size - 1);
        Assert.assertTrue(dRegExps != null && dRegExps.length == 2 * size - 1);
        this.size = size;
        this.xRegExps = xRegExps;
        this.yRegExps = yRegExps;
        this.dRegExps = dRegExps;
        this.xStrings = new Set[2 * size - 1];
        this.yStrings = new Set[2 * size - 1];
        this.dStrings = new Set[2 * size - 1];
    }

    public int getSize() {
        return size;
    }

    public String[] getxRegExps() {
        return xRegExps;
    }

    public String[] getyRegExps() {
        return yRegExps;
    }

    public String[] getdRegExps() {
        return dRegExps;
    }

    public Set<String>[] getxStrings() {
        return xStrings;
    }

    public Set<String>[] getyStrings() {
        return yStrings;
    }

    public Set<String>[] getdStrings() {
        return dStrings;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Vertical (xRegExps): ").append('\n');
        for (int a = 0; a < 2 * size - 1; a++) {
            sb.append(a);
            sb.append("\t").append(getLengthByArrayCoord(a, size));
            sb.append("\t").append(xRegExps[a]);
            sb.append('\n');
        }
        sb.append("Horizontal (yRegExps): ").append('\n');
        for (int a = 0; a < 2 * size - 1; a++) {
            sb.append(a);
            sb.append("\t").append(getLengthByArrayCoord(a, size));
            sb.append("\t").append(yRegExps[a]);
            sb.append('\n');
        }
        sb.append("Diagonal (dRegExps): ").append('\n');
        for (int a = 0; a < 2 * size - 1; a++) {
            sb.append(a);
            sb.append("\t").append(getLengthByArrayCoord(a, size));
            sb.append("\t").append(dRegExps[a]);
            sb.append('\n');
        }
        return sb.toString();
    }

    public void generateStrings(String[] regexps, Set<String>[] strs, int limit) {
        for (int x = -size + 1; x < size; x++) {
            int i = gridToArrayCoord(x, size);
            int length = getLengthByGridCoord(x, size);
            String exp = restrictRegExpToASCIIAlphabet(regexps[i]);

            Set<String> strings = getStrings(exp, length, limit);

            boolean add = !strings.isEmpty() && strings.size() < limit;

            if (add) {
                strs[i] = strings;
//                System.out.println(i + ":\t" + strings.size() + " ✔\t" + exp);
            }
        }
    }

    public void generateStrings() {
        generateStrings(1000);
    }

    /**
     * Generates a set of matching strings of correct length for each regular
     * expression. Note: No strings can be generated for regexps that contain
     * groups because the underlying automata implementation (brics) doesn't
     * support them.
     *
     * @param limit If the number of generated strings for a particular regexp
     * exceeds the limit the string set to null. This filters out regexp with
     * huge amounts of possible strings.
     */
    public void generateStrings(int limit) {
        System.out.println("Generating sets of strings from regexps (limit " + limit + ")");
        System.out.print("vertical");
        generateStrings(xRegExps, xStrings, limit);
        System.out.println(" ✔");
        System.out.print("horizontal");
        generateStrings(yRegExps, yStrings, limit);
        System.out.println(" ✔");
        System.out.print("diagonal");
        generateStrings(dRegExps, dStrings, limit);
        System.out.println(" ✔\n");
    }

    public static Set<String> getStrings(String regex, int length, int limit) {
        Set<String> strings;
        if (regex.contains("\\")) {
            strings = new HashSet();
        } else {
            Automaton a = new RegExp(regex).toAutomaton();
            strings = a.getStrings(length, limit);
        }
        return strings;
    }

    /**
     * intersects the regexp with [A-Z] character class cutting off unnecessary
     * unicode chars.
     *
     * @param exp
     * @return
     */
    public static String restrictRegExpToASCIIAlphabet(String exp) {
        return "[A-Z]*&".concat(exp.replaceAll("\\.", "[A-Z]"));
    }

    /**
     * Appends regular expressions using intersection operator: &. This
     * restricts a number of matching strings.
     *
     * @param constraints
     * @return
     */
    public Constraints intersectWithRegExpsFrom(Constraints constraints) {
        Assert.assertTrue(constraints.getSize() == size);

        for (int a = 0; a < 2 * size - 1; a++) {
            xRegExps[a] += "&" + constraints.getxRegExps()[a];
        }
        for (int a = 0; a < 2 * size - 1; a++) {
            yRegExps[a] += "&" + constraints.getyRegExps()[a];
        }
        for (int a = 0; a < 2 * size - 1; a++) {
            dRegExps[a] += "&" + constraints.getdRegExps()[a];
        }

        return this;
    }

    public void verifyStringsAgainst(Constraints constraints) {

        Assert.assertEquals(constraints.getSize(), size);

        System.out.println("Verifying vertical results");
        for (int a = 0; a < 2 * size - 1; a++) {
            Assert.assertNotNull(xStrings[a]);
            Assert.assertEquals(1, xStrings[a].size());
            String result = xStrings[a].iterator().next();
            String regexp = constraints.getxRegExps()[a];
            System.out.print("verifying: " + result + " with regexp: " + regexp);
            Assert.assertTrue(result.matches(regexp));
            System.out.println("  ✔");
        }
        System.out.println("Verifying horizontal results");
        for (int a = 0; a < 2 * size - 1; a++) {
            Assert.assertNotNull(yStrings[a]);
            Assert.assertEquals(1, yStrings[a].size());
            String result = yStrings[a].iterator().next();
            String regexp = constraints.getyRegExps()[a];
            System.out.print("verifying: " + result + " with regexp: " + regexp);
            Assert.assertTrue(result.matches(regexp));
            System.out.println("  ✔");
        }
        System.out.println("Verifying diagonal results");
        for (int a = 0; a < 2 * size - 1; a++) {
            Assert.assertNotNull(dStrings[a]);
            Assert.assertEquals(1, dStrings[a].size());
            String result = dStrings[a].iterator().next();
            String regexp = constraints.getdRegExps()[a];
            System.out.print("verifying: " + result + " with regexp: " + regexp);
            Assert.assertTrue(result.matches(regexp));
            System.out.println("  ✔");
        }

        System.out.println("\nResults verified. ✔");

    }

}
