package tkyjovsk.hexpuzzle;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author tkyjovsk
 */
public class Field {

    public static final String ALL_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final Set<Character> characters = new HashSet<>();

    private final int x;
    private final int y;
    private final int d;

    public Field(int x, int y) {
        this.x = x;
        this.y = y;
        this.d = HexaGrid.getDCoord(x, y);
        for (int i = 0; i < ALL_CHARACTERS.length(); i++) {
            characters.add(ALL_CHARACTERS.charAt(i));
        }
    }

    DecimalFormat df = new DecimalFormat("##");

    @Override
    public String toString() {
//        return getValidCharacters().size() + " ";
        return df.format(x) + "." + df.format(y) + "." + df.format(d);
    }

    /** Creates regular expression that matches all characters.
     * 
     * @return 
     */
    public String toRegExp() {
        StringBuilder sb = new StringBuilder();
        if (characters.size() > 1) {
            sb.append('[');
        }
        for (Character ch : characters) {
            sb.append(ch);
        }
        if (characters.size() > 1) {
            sb.append(']');
        }
        return sb.toString();
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getD() {
        return d;
    }

}
