package tkyjovsk.hexpuzzle;

/**
 * A set of known fields. These were deduced manually (from the regexps 
 * that contain groups and are not supported by brics automaton) 
 * based on previous incomplete computed solutions.
 * 
 * @author tkyjovsk
 */
public class KnownFields {

    public static void setKnownField(HexaGrid grid, int x, int y, char c) {
        grid.getFieldByGridCoords(x,y).getCharacters().clear();
        grid.getFieldByGridCoords(x,y).getCharacters().add(c);
    }
    
    public static void injectIntoGrid(HexaGrid grid) {
        System.out.println("Injecting known fields.");
        
        setKnownField(grid, -6,-4, 'F');
        
        setKnownField(grid, -2,4, 'N');
        
        setKnownField(grid, 6,4, 'E');
        
        setKnownField(grid, -5,1, 'O');
        setKnownField(grid, -4,1, 'R');
        setKnownField(grid, -3,1, 'E');
        
        setKnownField(grid, -2,1, 'O');
        setKnownField(grid, -1,1, 'R');
        setKnownField(grid, 0,1, 'E');
        setKnownField(grid, 1,1, 'O');
        setKnownField(grid, 2,1, 'R');
        setKnownField(grid, 3,1, 'E');
        setKnownField(grid, 4,1, 'O');
        setKnownField(grid, 5,1, 'R');
        setKnownField(grid, 6,1, 'E');
        
        setKnownField(grid, -2,-4, 'X');
        setKnownField(grid, 0,-2, 'C');
        
        setKnownField(grid, 1,2, 'H');
        setKnownField(grid, 3,4, 'E');
        setKnownField(grid, 4,5, 'M');
        setKnownField(grid, 5,6, 'C');
        
        setKnownField(grid, -4,-6, 'P');
        setKnownField(grid, -4,-4, 'X');
        setKnownField(grid, -4,-3, 'O');
    }
    
}
